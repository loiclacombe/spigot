FROM openjdk:8-jdk-alpine as build

WORKDIR /build

ENV SPIGOT_REV latest

RUN apk add --no-cache wget git
RUN wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar && \
  java -jar BuildTools.jar --rev $SPIGOT_REV && \
  mv spigot-*.jar spigot.jar && \
  mv craftbukkit-*.jar craftbukkit.jar 

FROM openjdk:8-jre-alpine

COPY --from=build /build/spigot.jar /spigot/spigot.jar

VOLUME /data
EXPOSE 25565

WORKDIR /data

CMD ["/usr/bin/java", "-Xms512M", "-Xmx1536M", "-server", "-jar", "/spigot/spigot.jar", "--noconsole"]